# Use the official Node.js 16 as a parent image
FROM node:lts-alpine

# RUN apt-get update && apt-get upgrade -y && && rm -rf /var/lib/apt/lists/*

RUN apk update && apk upgrade

USER node

# Set the working directory to /app/backend within the container
WORKDIR /app/backend

# Copy package.json and package-lock.json (or other package manager files) to the container
COPY --chown=node:node ./package*.json ./

# Install backend dependencies
RUN npm install

# Copy the backend directory contents into the container at /app/backend
COPY --chown=node:node ./backend backend

# Make port 5000 available to the world outside this container
EXPOSE 5000

# Run the app when the container launches
CMD ["npm", "start"]
