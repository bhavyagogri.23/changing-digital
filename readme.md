# Documentation

## Online Setup

The site is accessible on http://prosho-publi-3qjmtxjkr9d4-1398119004.ap-south-1.elb.amazonaws.com/
There are 2 services, frontend and backend, both are of type Load Balanced Web Service using AWS Copilot, hosted on ECS (Fargate).

Both the frontend and backend have been configured with 2 replicas minimum and 3 maximum. The reason for 2 minimum is for higher availability.

## Prerequisites

- AWS Account with Admin and IAM Access. (Root user does not work with Copilot)
- Docker

## Local Setup

There is a docker-compose.yml file which can be used to setup the application locally, by executing 
`docker compose up` or `docker-compose up` depending on the version of docker and docker compose being used.

## MongoDB

Mongodb atlas has been used as it was recommended in the documentation of aws copilot https://aws.github.io/copilot-cli/community/guides/#code-samples. 

## Backend Dockerfile

Docker base image of `node:lts-alpine` (lts) for its long support cycle and (alpine) for less secuity issues. A stage to update the image packages has been added. The container will run with a non root user.

## Frontend Dockerfile

The dockerfile is a multistage one. In the build stage a static build is made and is copied to an nginx:alpine docker image. This helps reduce size and improve security. 

## Copilot

The copilot manifests are stored in the copilot folder with 1 application named `proshop` 1 environment named `c4`. There are 2 services (backend and frontend). For backend the manifest is indie `copilot/backend` named `manifest.yml`. For frontend the manifest is indie `copilot/frontend` named `manifest.yml` 

## Copilot Setup

```
export AWS_PROFILE=some_profile
copilot app init proshop
copilot env init --name test --profile <some_profile> --default-config
copilot env deploy --name test
copilot svc init --name backend --svc-type "Load Balanced Web Service" --dockerfile ./backend.Dockerfile
copilot svc deploy --name backend --env test
copilot svc init --name frontend --svc-type "Load Balanced Web Service" --dockerfile ./frontend.Dockerfile
copilot svc deploy --name frontend --env test
```

## CI / CD

The CI/CD pipeline is setup using Gitlab CI/CD. The pipeline is defined in the `.gitlab-ci.yml` file. The pipeline is triggered on every push to the main branch. copilot svc deploy is used to trigger the build of the docker images, push the images to ECR and deploy them to the ECS cluster.

## Configurations

There is an nginx.conf file which helps with the local docker compose setup and also with directly opening urls like http://prosho-publi-3qjmtxjkr9d4-1398119004.ap-south-1.elb.amazonaws.com/login or http://prosho-publi-3qjmtxjkr9d4-1398119004.ap-south-1.elb.amazonaws.com/cart. The specific part is `try_files $uri $uri/ /index.html;`

In the generateTokens.js token is set to `secure: 'false',` for it work with non HTTPS setups. In a production environment this will be set to true and setup SSL certificates using AWS Certificate Manager or something else.

ECR is used to store docker images.
