# Stage 1: Build the React application
FROM node:lts as build

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json (or yarn.lock) files
COPY frontend/package*.json ./
# Alternatively, if you're using yarn, you might want to copy the yarn.lock file
# COPY package.json yarn.lock ./

# Install dependencies
RUN npm install
# Or use yarn install if you're using yarn
# RUN yarn install

# Copy the rest of your app's source code from your host to your image filesystem.
COPY frontend/ .

# Build the application
RUN npm run build
# Or use yarn build if you're using yarn
# RUN yarn build

# Stage 2: Serve the application from Nginx
FROM nginx:alpine

RUN apk update && apk upgrade

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# Copy the built application from the previous stage
COPY --from=build /app/build /usr/share/nginx/html

# Copy nginx configuration (optional)
# If you have a custom nginx.conf, you can uncomment the next line and provide the file
# COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose port 80 to the host
EXPOSE 80

# Start Nginx and keep the process from backgrounding and the container from quitting
# CMD ["nginx", "-g", "daemon off;"]
