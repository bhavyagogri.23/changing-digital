# Documentation

## Online Setup

The site is accessible on http://prosho-Publi-Kk9Z9W2aiJOD-6081011.ap-south-1.elb.amazonaws.com
There are 2 services, frontend and backend, both are of type Load Balanced Web Service, I wanted to go for Static with the frontend, but I wanted to get everything up and running (I found Load Balanced Web Service easier) and then do the switch to Static if needed later.

Both the frontend and backend have been configured with 2 replicas minimum and 3 maximum. The reason for 2 minimum is for higher availability.

## Local Setup

There is a docker-compose.yml file which can be used to setup the application locally, by executing `docker compose up` or `docker-compose up` depending on the version of docker and docker compose being used.

## MongoDB

I have used mongodb atlas as it was recommended in the documentation of aws copilot https://aws.github.io/copilot-cli/community/guides/#code-samples. 

## Backend Dockerfile

I have used node:lts-alpine (lts) for its long support cycle and (alpine) for less secuity issues. Added a stage to update the image packages before pushing. The container will run with a  non root user.

## Frontend Dockerfile

I have used multistage docker. In the build stage a static build is made and is copied to an nginx:alpine docker image. This helps reduce size and improve security. There is an nginx.conf file which helps with the local docker compose setup and also with directly opening urls like http://prosho-Publi-Kk9Z9W2aiJOD-6081011.ap-south-1.elb.amazonaws.com/login or http://prosho-Publi-Kk9Z9W2aiJOD-6081011.ap-south-1.elb.amazonaws.com/cart. The specific part is `try_files $uri $uri/ /index.html;`

